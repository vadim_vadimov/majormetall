<?php

	function theme_scripts() {

		wp_enqueue_style( 'styles', get_template_directory_uri() . '/assets/css/style.css' );

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/vendor.js', false, null, true );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', ['jquery'], null, true );
	}

	function theme_main_setup() {

		register_nav_menus( array(
			'header_menu' => 'Menu in header',
		));
	}

	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Настройки Темы',
			'menu_title'	=> 'Настройки Темы',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));	
	}

	/* Support SVG
	=======================================*/

	function cc_mime_types( $mimes ){
	    $mimes['svg'] = 'image/svg+xml';
	    return $mimes;
	}
	add_filter( 'upload_mimes', 'cc_mime_types' );





	/* Remove update notification  in ACF plugin
	=======================================*/

	function filter_plugin_updates( $value ) {
	    unset( $value->response['advanced-custom-fields-pro-master/acf.php'] );
	    return $value;
	}
	add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );



	/* Remove br from CONTACT FORM 7
	=======================================*/
	add_filter('wpcf7_autop_or_not', '__return_false');



	
	/* Init Custom Post type
	=======================================*/

	include (get_template_directory() . '/assets/inc/custom-post-type-catalog.php');
	include (get_template_directory() . '/assets/inc/custom-post-type-review.php');

	



	add_action('wp_enqueue_scripts', 'theme_scripts');
	add_action('after_setup_theme', 'theme_main_setup');



add_filter( 'jetpack_implode_frontend_css', '__return_false', 99 );

