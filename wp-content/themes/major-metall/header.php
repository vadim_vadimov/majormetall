<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php if (is_page(374)) { ?>
	    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=6f1a18d1-b19e-4aa0-bef1-91a758b146a2" type="text/javascript"></script>
	<?php }  ?>

	<title>
        <?php wp_title(''); ?>
	</title>

	<link rel="icon" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon.ico" type="image/x-icon">

	<?php wp_head(); ?>
</head>
<body>
	<header class="header">
		<div class="header__top">
			<div class="header__info">

				<?php if (get_field('address', 'options')) { ?>
					<div class="header__info-item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/geo.svg" alt="img">
						<span class="header__info-text"><?php the_field('address', 'options') ?></span>
					</div>	
				<?php } ?>	

				<?php if (get_field('email', 'options')) { ?>
					<div class="header__info-item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/mail.svg" alt="img">
						<a href="mailto:<?php the_field('email', 'options') ?>" class="header__info-text"><?php the_field('email', 'options') ?></a>
					</div>	
				<?php } ?>	

				<?php if (get_field('tel', 'options')) { ?>
					<div class="header__info-item  header__info-item--tel">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/tel.svg" alt="img">
						<a href="tel:<?php the_field('tel', 'options') ?>" class="header__info-text"><?php the_field('tel', 'options') ?></a>
					</div>	
				<?php } ?>	
				
				<a data-fancybox data-src="#popup-request" href="javascript://" class="btn-order  btn-order-mobile">Оформить заявку</a>	
			</div>	
		</div>
		<div class="header__bottom">
			<div class="header__content">

				<?php if (get_field('logo_h', 'options')) { ?>
					<a href="<?php echo esc_url( home_url() ); ?>" class="header__logo">
						<img src="<?php the_field('logo_h', 'options') ?>" alt="logo">
					</a>
				<?php } ?>	

				<nav class="header__menu">

					<?php wp_nav_menu(array(
						'theme_location'  => 'header_menu',
						'container'       => null, 
						'menu_class'      => 'header__menu-list', 
					)); ?>

				</nav>
				<a data-fancybox data-src="#popup-request" href="javascript://" class="btn-order">Оформить заявку</a>	
				<div class="burger-content">
					<div class="burger-mob"></div>
				</div>
			</div>
		</div>
	</header>