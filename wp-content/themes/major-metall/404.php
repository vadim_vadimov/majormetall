<?php get_header(); ?>
	
	<div class="page-zero">
		<div class="container">
			<span class="page-zero__title">404</span>
			<p class="page-zero__text">Извините! Страница, которую Вы ищете, не может быть найдена</p>
		</div>
	</div>
	
<?php get_footer(''); ?>