	<footer class="footer">
		<div class="footer__top">
			<div class="container">
				<div class="footer__top-content">
					<div class="footer__logo-wrap">

						<?php if (get_field('logo_f', 'options')) { ?>

							<a href="<?php echo esc_url( home_url() ); ?>" class="footer__logo">
								<img src="<?php the_field('logo_f', 'options') ?>" alt="img">
							</a>

						<?php } ?>	

						<p>ООО "Мейджор металл" © <?php echo date("Y"); ?></p>
					</div>
					<div class="header__info">

						<?php if (get_field('address', 'options')) { ?>
							<div class="header__info-item">
								<img src="<?php echo get_template_directory_uri() ?>/assets/img/geo.svg" alt="img">
								<span class="header__info-text"><?php the_field('address', 'options') ?></span>
							</div>	
						<?php } ?>	

						<?php if (get_field('email', 'options')) { ?>
							<div class="header__info-item">
								<img src="<?php echo get_template_directory_uri() ?>/assets/img/mail.svg" alt="img">
								<a href="mailto:<?php the_field('email', 'options') ?>" class="header__info-text"><?php the_field('email', 'options') ?></a>
							</div>	
						<?php } ?>	

						<?php if (get_field('tel', 'options')) { ?>
							<div class="header__info-item  header__info-item--tel">
								<img src="<?php echo get_template_directory_uri() ?>/assets/img/tel.svg" alt="img">
								<a href="tel:<?php the_field('tel', 'options') ?>" class="header__info-text"><?php the_field('tel', 'options') ?></a>
							</div>	
						<?php } ?>	

					</div>	
				</div>
			</div>
		</div>

		<?php if (get_field('footer_text', 'options')) { ?>

			<div class="footer__bottom">
				<div class="container">
					<?php the_field('footer_text', 'options') ?>
				</div>
			</div>
		
		<?php } ?>	

	</footer>

	<?php if (get_field('main-form_popup', 'options')) { ?>
		<div class="popup" id="popup-request">

			<?php if (get_field('main-form_popup-title', 'options')) { ?>
				<div class="main-title__wrap">
					<h3 class="main-title"><?php the_field('main-form_popup-title', 'options') ?></h3>
				</div>
			<?php } ?> 

			<?php the_field('main-form_popup', 'options') ?>

		</div>
	<?php } ?> 

	<?php if (get_field('main-form_review', 'options')) { ?>
		<div class="popup  popup-review" id="popup-review">

			<?php if (get_field('main-form_review-title', 'options')) { ?>
				<div class="main-title__wrap">
					<h3 class="main-title"><?php the_field('main-form_review-title', 'options') ?></h3>
				</div>
			<?php } ?> 

			<?php the_field('main-form_review', 'options') ?>

		</div>
	<?php } ?> 
	
	<?php wp_footer(); ?>

</body>
</html>