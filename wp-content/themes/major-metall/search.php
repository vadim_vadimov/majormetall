<?php get_header(); ?>

<div class="search-inner">
	<section class="inner-title">
	    <div class="container">
	    	<div class="inner-title__content">
	    	    <div class="square-mask"></div>
	    	    <h2 class="inner-title__title">Результаты Поиска</h2>
	    	</div>
	    </div>
	</section>
	
	<div class="inner  inner-article">
		<div class="container">
			
			<?php
			    global $query_string;
			    $query_args = explode("&", $query_string);
			    $search_query = array('post_type' => 'catalog',
			    					'posts_per_page' => 40,
			    					'order' => 'DESC');

			    foreach($query_args as $key => $string) {
			      $query_split = explode("=", $string);
			      $search_query[$query_split[0]] = urldecode($query_split[1]);
			    } // foreach

			    $page_index = new WP_Query($search_query);
			    if ( $page_index->have_posts() && get_search_query() ) : 
			    ?>

			    <!-- the loop -->

			    <div class="catalog__content  catalog__content--search">
			    	<div class="catalog__list">

			    		<?php while ( $page_index->have_posts() ) : $page_index->the_post(); ?>

			    		    <div class="catalog__item-wrap">
			    		        <a href="<?php echo esc_url( get_permalink() ); ?>" class="catalog__item">

			    		            <?php if (get_field('single_img')) { ?>
			    		                <div class="catalog__item-img">
			    		                    <img src="<?php the_field('single_img') ?>" alt="img">
			    		                </div>
			    		            <?php } ?>  

			    		            <span class="catalog__item-name"><?php esc_html( the_title() ); ?></span>
			    		        </a>
			    		    </div>

			    		<?php endwhile; ?>
			    		
			    	</div>
			    </div>

			    <!-- end of the loop -->

			    <?php wp_reset_postdata(); ?>


			<?php else : ?>

				<?php get_search_form(); ?>

				<div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full  main-title__wrap--search">
				    <h3 class="main-title"><?php _e( 'По вашему запросу ничего не найдено. Попробуйте еще раз' ); ?></h3>
				</div>

			<?php endif; ?>

		</div>
	</div>
</div>
	
<?php get_footer(); ?>