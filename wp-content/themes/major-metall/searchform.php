<div class="search-content">
    <div class="container">
    	<form class="search-content__form" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
    		<div class="main-form__field  main-form__field--full">
                <input type="text" value="<?php echo get_search_query() ?>" name="s" id="s" placeholder="Поиск по каталогу">
            </div>
    		
    		<button id="searchsubmit" class="btn-item  btn-main">Найти</button>
    	</form>
    </div>
</div>