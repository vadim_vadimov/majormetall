<?php get_header(); ?>

	<section class="single-page">
		<div class="container">
			<div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full">
				<h3 class="main-title"><?php echo esc_html( get_the_title() ); ?></h3>
			</div>
			<div class="single-page__goods">

				<?php if (get_field('single_img')) { ?>
					<div class="single-page__goods-img">
						<img src="<?php the_field('single_img') ?>" alt="img">
					</div>
				<?php } ?>  

				<?php if (get_field('single_descr')) { ?>
				    <div class="single-page__goods-text">
				    	<?php the_field('single_descr') ?>
				    	<a data-fancybox data-src="#popup-request" href="javascript://" class="btn-item  btn-main  btn-main--left">Оформить заявку</a>
				    </div>
				<?php } ?>  
				
			</div>

			<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

				<div class="single-page__content">
					<?php echo esc_html( the_content() ); ?>
				</div>

			<?php endwhile; ?>
			<?php endif; ?>	

			<?php if( have_rows('table-single') ): ?>   
			    <div class="single-page__table-content">
			        
			        <?php while( have_rows('table-single') ): the_row(); 
			            $title = get_sub_field('table-single_title');
			            $table = get_sub_field('table-single_content');

			            ?>

			            <h4><?php echo $title; ?></h4>
			            <div class="single-page__table-wrap">
			            	
			            	<?php 
			            		$table = get_sub_field( 'table-single_content' );

			            		if ( ! empty ( $table ) ) {

			            		    echo '<table border="0">';

			            		        if ( ! empty( $table['caption'] ) ) {

			            		            echo '<caption>' . $table['caption'] . '</caption>';
			            		        }

			            		        if ( ! empty( $table['header'] ) ) {

			            		            echo '<thead>';

			            		                echo '<tr>';

			            		                    foreach ( $table['header'] as $th ) {

			            		                        echo '<th>';
			            		                            echo $th['c'];
			            		                        echo '</th>';
			            		                    }

			            		                echo '</tr>';

			            		            echo '</thead>';
			            		        }

			            		        echo '<tbody>';

			            		            foreach ( $table['body'] as $tr ) {

			            		                echo '<tr>';

			            		                    foreach ( $tr as $td ) {

			            		                        echo '<td>';
			            		                            echo $td['c'];
			            		                        echo '</td>';
			            		                    }

			            		                echo '</tr>';
			            		            }

			            		        echo '</tbody>';

			            		    echo '</table>';
			            		}
			            	?>
			            </div>

			        <?php endwhile; ?>  
			    </div>
			<?php endif; ?> 	

		</div>
	</section>

	<?php get_template_part( 'template-parts/contact-form' ); ?>

<?php get_footer(); ?>