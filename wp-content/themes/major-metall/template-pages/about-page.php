<?php 
/**
 *	Template name: About Page 
 */

get_header(); ?>

    <?php get_template_part( 'template-parts/inner-title' ); ?>

    <section class="about-info">
        <div class="container">
            <div class="about-info__content">

                <?php if (get_field('about_main-img')) { ?>
                    <div class="about-info__img" style="background-image: url(<?php the_field('about_main-img') ?>);">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/ai-logo.svg" alt="img">
                    </div>
                <?php } ?>  
                
                <div class="about-info__text">

                    <?php if (get_field('about_main-title')) { ?>
                        <h3 class="main-title"><?php the_field('about_main-title') ?></h3>
                    <?php } ?>  

                    <?php if (get_field('about_main-text')) { ?>
                        <?php the_field('about_main-text') ?>
                    <?php } ?>  
                </div>
            </div>
        </div>
    </section>

    <section class="about-benefits">
        <div class="container">

            <?php if( have_rows('why-we-list', 7) ): ?>   
                <div class="why-we__list  items-list">
            
                <?php while( have_rows('why-we-list', 7) ): the_row(); 
                    $image = get_sub_field('why-we-list_img');
                    $title = get_sub_field('why-we-list_title');

                    ?>

                    <div class="items-list__item-wrap">
                        <div class="why-we__item">
                            <div class="why-we__item-img">
                                <img src="<?php echo $image; ?>" alt="img">
                            </div>
                            <p><?php echo $title; ?></p>
                        </div>
                    </div>

                <?php endwhile; ?>  

                </div>
            <?php endif; ?> 

        </div>
    </section>

    <section class="proposal">
        <div class="container">
            <div class="proposal__content">
                <div class="proposal__text">

                    <?php if (get_field('review-inner_title')) { ?>
                        <div class="main-title__wrap  main-title__wrap--left">
                            <h3 class="main-title"><?php the_field('review-inner_title') ?></h3>
                        </div>
                    <?php } ?>  

                    <?php if (get_field('review-inner_text')) { ?>
                        <p class="main-title__description  main-title__description--left"><?php the_field('review-inner_text') ?></p>
                    <?php } ?>  
                    
                    <a data-fancybox data-src="#popup-review" href="javascript://" class="btn-item  btn-main  btn-main--left">Оставить отзыв</a>
                </div>

                <?php if (get_field('review-inner_img')) { ?>
                    <div class="proposal__img" style="background-image: url(<?php the_field('review-inner_img') ?>);"></div>
                <?php } ?>
                
            </div>
        </div>
    </section>

    <div class="partners  partners-inner">
        <div class="container">
            
            <?php if( have_rows('partners-list', 7) ): ?>   
                <div class="partners__list">
            
                <?php while( have_rows('partners-list', 7) ): the_row(); 
                    $image = get_sub_field('partners-list-img');

                    ?>

                    <div class="partners__item-wrap">
                        <div class="partners__item">
                            <img src="<?php echo $image; ?>" alt="img">
                        </div>
                    </div>
                    
                <?php endwhile; ?>  

                </div>
            <?php endif; ?> 

        </div>
    </div>

<?php get_footer(); ?>