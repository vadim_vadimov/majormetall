<?php 
/**
 *	Template name: Contact Page 
 */

get_header(); ?>

    <?php get_template_part( 'template-parts/inner-title' ); ?>

    <div class="contacts-inner">
        <div class="container">
            <div class="contacts-inner__content">
                <div class="contacts-inner__map-wrap">
                    <div id="map-1"></div>
                </div>
                <div class="contacts-inner__info">
                    <div class="contacts-inner__item">
                        <div class="contacts-inner__title-content">
                            <div class="contacts-inner__img">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/geo.svg" alt="img">
                            </div>
                            <strong class="contacts-inner__title">Адрес</strong>
                        </div>

                        <?php if (get_field('contact_address')) { ?>
                           <?php the_field('contact_address') ?>
                        <?php } ?>  
                       
                    </div>
                    <div class="contacts-inner__item">
                        <div class="contacts-inner__title-content">
                            <div class="contacts-inner__img">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/mail-c.svg" alt="img">
                            </div>
                            <strong class="contacts-inner__title">Почтовый адрес</strong>
                        </div>

                        <?php if (get_field('contact_index')) { ?>
                           <?php the_field('contact_index') ?>
                        <?php } ?>  

                    </div>
                    <div class="contacts-inner__item">
                        <div class="contacts-inner__title-content">
                            <div class="contacts-inner__img">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/tel-c.svg" alt="img">
                            </div>
                            <strong class="contacts-inner__title">Телефоны</strong>
                        </div>

                        <?php if (get_field('contact_tel')) { ?>
                           <?php the_field('contact_tel') ?>
                        <?php } ?>  
                        
                    </div>
                    <div class="contacts-inner__item">
                        <div class="contacts-inner__title-content">
                            <div class="contacts-inner__img">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/time-c.svg" alt="img">
                            </div>
                            <strong class="contacts-inner__title">Режим работы</strong>
                        </div>

                        <?php if (get_field('contact_work')) { ?>
                           <?php the_field('contact_work') ?>
                        <?php } ?>  

                    </div>
                    <div class="contacts-inner__item  contacts-inner__item--mail">
                        <div class="contacts-inner__title-content">
                            <div class="contacts-inner__img">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/email-c.svg" alt="img">
                            </div>
                            <strong class="contacts-inner__title">E-mail</strong>
                        </div>

                        <?php if (get_field('contact_email')) { ?>
                           <?php the_field('contact_email') ?>
                        <?php } ?>  
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>