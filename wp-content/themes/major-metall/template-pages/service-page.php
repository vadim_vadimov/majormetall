<?php 
/**
 *	Template name: Servire Page
 */

get_header(); ?>

    <?php get_template_part( 'template-parts/inner-title' ); ?>

    <section class="service-inner">
        <div class="container">

	        <?php
            $page_text = get_the_content();
	        if ( !empty($page_text) ) { ?>

                <div class="editor-content">
	                <?php the_content(); ?>
                </div>

	        <?php } ?>

            <?php if (get_field('service-title')) { ?>
                <div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full">
                    <h3 class="main-title"><?php the_field('service-title') ?></h3>
                </div>
            <?php } ?>  
            
            <?php if (get_field('service-text')) { ?>
                <p class="main-title__description  main-title__description--left   main-title__description--full"><?php the_field('service-text') ?></p>
            <?php } ?>  

            <?php if( have_rows('cars-list') ): ?>   
                <div class="product__list  delivery__list">
            
                <?php while( have_rows('cars-list') ): the_row(); 
                    $img = get_sub_field('cars-list_img');
                    $title = get_sub_field('cars-list_name');

                    ?>

                    <div class="product__item-wrap">
                        <div class="service__product-item">
                            <div class="product__item" style="background-image: url(<?php echo $img; ?>);">
                                <h4 class="product__item-title"><?php echo $title; ?></h4>
                            </div>

                            <?php if( have_rows('cars-list_text') ): ?>   
                                <ul class="product__item-service-list">
                            
                                <?php while( have_rows('cars-list_text') ): the_row(); 
                                    $text_item = get_sub_field('cars-list_text-item');

                                    ?>

                                    <li class="product__item-service-item"><?php echo $text_item; ?></li>

                                <?php endwhile; ?>  

                                </ul>
                            <?php endif; ?> 
  
                        </div>
                    </div>

                <?php endwhile; ?>  

                 </div>
            <?php endif; ?> 

            <?php if (get_field('service-table_title')) { ?>
                <div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full">
                    <h3 class="main-title"><?php the_field('service-table_title') ?></h3>
                </div>
            <?php } ?>  

            <div class="single-page__table-content">
                <div class="single-page__table-wrap">

                    <?php 
                        $table = get_field( 'service-table_list' );

                        if ( ! empty ( $table ) ) {

                            echo '<table border="0">';

                                if ( ! empty( $table['caption'] ) ) {

                                    echo '<caption>' . $table['caption'] . '</caption>';
                                }

                                if ( ! empty( $table['header'] ) ) {

                                    echo '<thead>';

                                        echo '<tr>';

                                            foreach ( $table['header'] as $th ) {

                                                echo '<th>';
                                                    echo $th['c'];
                                                echo '</th>';
                                            }

                                        echo '</tr>';

                                    echo '</thead>';
                                }

                                echo '<tbody>';

                                    foreach ( $table['body'] as $tr ) {

                                        echo '<tr>';

                                            foreach ( $tr as $td ) {

                                                echo '<td>';
                                                    echo $td['c'];
                                                echo '</td>';
                                            }

                                        echo '</tr>';
                                    }

                                echo '</tbody>';

                            echo '</table>';
                        }
                    ?>

                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>