<?php 
/**
 *	Template name: Catalog Page 
 */

get_header(); ?>

    <?php get_template_part( 'template-parts/inner-title' ); ?>

    <?php get_search_form(); ?>

    <section class="catalog">
        <div class="container">
            <div class="catalog__content">
                <div class="catalog__title" style="background-image: url(<?php the_field('cat-t-1') ?>);">
                    <h4 class="product__item-title">СОРТОВОЙ ПРОКАТ</h4>
                </div>

                <?php $args = array('post_type' => 'catalog',
                                    'posts_per_page' => 40,
                                    'category_name' => 'long-products',
                                    'order' => 'DESC') ?>

                <?php $page_index = new WP_Query($args) ?>

                <div class="catalog__list">

                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="catalog__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="catalog__item">

                            <?php if (get_field('single_img')) { ?>
                                <div class="catalog__item-img">
                                    <img src="<?php the_field('single_img') ?>" alt="img">
                                </div>
                            <?php } ?>  

                            <span class="catalog__item-name"><?php esc_html( the_title() ); ?></span>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

                </div>
                <?php wp_reset_postdata(); ?>       

            </div>
            <div class="catalog__content">
                <div class="catalog__title" style="background-image: url(<?php the_field('cat-t-2') ?>);">
                    <h4 class="product__item-title">ЛИСТОВОЙ ПРОКАТ</h4>
                </div>
                
                <?php $args = array('post_type' => 'catalog',
                                    'posts_per_page' => 40,
                                    'category_name' => 'sheet-rent',
                                    'order' => 'DESC') ?>

                <?php $page_index = new WP_Query($args) ?>

                <div class="catalog__list">

                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="catalog__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="catalog__item">

                            <?php if (get_field('single_img')) { ?>
                                <div class="catalog__item-img">
                                    <img src="<?php the_field('single_img') ?>" alt="img">
                                </div>
                            <?php } ?>  

                            <span class="catalog__item-name"><?php esc_html( the_title() ); ?></span>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

                </div>
                <?php wp_reset_postdata(); ?>       

            </div>
            <div class="catalog__content">
                <div class="catalog__title" style="background-image: url(<?php the_field('cat-t-3') ?>);">
                    <h4 class="product__item-title">МЕТИЗНАЯ ПРОДУКЦИЯ</h4>
                </div>
                
                <?php $args = array('post_type' => 'catalog',
                                    'posts_per_page' => 40,
                                    'category_name' => 'furniture-prod',
                                    'order' => 'DESC') ?>

                <?php $page_index = new WP_Query($args) ?>

                <div class="catalog__list">

                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="catalog__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="catalog__item">

                            <?php if (get_field('single_img')) { ?>
                                <div class="catalog__item-img">
                                    <img src="<?php the_field('single_img') ?>" alt="img">
                                </div>
                            <?php } ?>  

                            <span class="catalog__item-name"><?php esc_html( the_title() ); ?></span>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

                </div>
                <?php wp_reset_postdata(); ?>       
                
            </div>
        </div>
    </section>

<?php get_footer(); ?>