<?php 
/**
 *	Template name: Home Page 
 */

get_header(); ?>

    <section class="intro">
        <div class="container-1104">
            
            <?php if( have_rows('slider_intro') ): ?>   
                <div class="intro__slider-wrap">
                    <div class="slider-for  intro-sl-1">
            
                    <?php while( have_rows('slider_intro') ): the_row(); 
                        $image = get_sub_field('slider_i_img');
                        $title = get_sub_field('slider_i_title');
                        $text = get_sub_field('slider_i_text');

                        ?>

                        <div class="intro__slider-slide">
                            <div class="intro__slider-item">
                                <div class="intro__slider-img" style="background-image: url(<?php echo $image; ?>);"></div>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                    </div>

                    <div class="intro-sl-2__wrap">
                        <div class="slider-nav  intro-sl-2">

                        <?php while( have_rows('slider_intro') ): the_row(); 
                            $image = get_sub_field('slider_i_img');
                            $title = get_sub_field('slider_i_title');
                            $text = get_sub_field('slider_i_text');

                            ?>

                            <div class="intro__slider-slide">
                                <div class="intro__slider-content">
                                    <h3 class="main-title"><?php echo $title; ?></h3>
                                    <p><?php echo $text; ?></p>
                                </div>
                            </div>

                        <?php endwhile; ?>  
                        </div>
                        <div class="intro__slider-btns">
                            <button class="intro__slider-btn  intro__slider-btn--prev">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/sl-l.svg" alt="prev">
                            </button>
                            <button class="intro__slider-btn  intro__slider-btn--next">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/sl-r.svg" alt="next">
                            </button>
                        </div>
                    </div>
                </div>

                <div class="square-mask"></div>
            <?php endif; ?> 

        </div>
    </section>

    <section class="about">
        <div class="container-1104">
            <div class="about__content-wrap">
                <div class="about__content">

                    <?php if (get_field('block-title_2')) { ?>
                        <h3 class="main-title"><?php the_field('block-title_2') ?></h3>
                    <?php } ?>


	                <?php if( have_rows('block_2_slides') ): ?>
                        <div class="about-text-slides">
			                <?php
                            while( have_rows('block_2_slides') ): the_row();
				                $slide_text = get_sub_field('text');
				                ?>
                                <div>
                                    <div class="about-text-slide <?php if ($about_slides_i == 0) echo ' active'; ?>">
                                        <p><?php echo $slide_text; ?></p>
                                    </div>
                                </div>
			                <?php
                            endwhile; ?>
                        </div>
	                <?php endif; ?>

<!--                    --><?php //if (get_field('block-text_2')) { ?>
<!--                        <p>--><?php //the_field('block-text_2') ?><!--</p>-->
<!--                    --><?php //} ?><!-- -->

                    <?php if( have_rows('block_2_slides') ): ?>
                        <div class="about__numbers">
                            <div class="about__numbers-indicator"></div>

                            <?php
                            $about_nums_i = 0;
                            while( have_rows('block_2_slides') ): the_row();
                                $slide_number = get_sub_field('number');
                                ?>
                                <div class="about__num-item" data-num="<?php echo $about_nums_i; ?>">
                                    <span class="about__num-title"><?php echo $slide_number; ?></span>
    <!--                                <span class="about__num-text">--><?php //echo 'text'; ?><!--</span>-->
                                </div>

                            <?php
                                $about_nums_i++;
                            endwhile; ?>

                         </div>
                    <?php endif; ?> 
                    
                </div>

                <?php if (get_field('num-img')) { ?>
                    <div class="about__img" style="background-image: url(<?php the_field('num-img') ?>);">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/m.svg" alt="img">
                    </div>
                <?php } ?>  
                
            </div>
        </div>
    </section>

    <section class="scheme-work">
        <div class="container-1104">

            <?php if (get_field('block-title_3')) { ?>

                <div class="main-title__wrap">
                    <h3 class="main-title"><?php the_field('block-title_3') ?></h3>
                </div>

            <?php } ?>  

            <?php if( have_rows('shema-list') ): ?>   
                <div class="scheme-work__list-container">
                    <div class="scheme-work__list">
                    
                    <?php while( have_rows('shema-list') ): the_row(); 
                        $image = get_sub_field('shema-list_img');
                        $title = get_sub_field('shema-list_title');

                        ?>

                        <div class="scheme-work__item-wrap">
                            <div class="scheme-work__item">
                                <div class="scheme-work__item-img">
                                    <img src="<?php echo $image; ?>" alt="img">
                                </div>
                                <strong class="scheme-work__item-text"><?php echo $title; ?></strong>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                    
                    </div>
                </div>
            <?php endif; ?> 

        </div>
    </section>

    <section class="product">
        <div class="container">

            <?php if (get_field('block-title_5')) { ?>

                <div class="main-title__wrap">
                    <h3 class="main-title"><?php the_field('block-title_5') ?></h3>
                </div>

            <?php } ?>  

            <div class="product__list">
                <div class="product__item-wrap">
                    <div class="product__item" style="background-image: url(<?php the_field('cat-t-1', 255) ?>);">
                        <h4 class="product__item-title">СОРТОВОЙ ПРОКАТ</h4>
                        <a href="<?php echo esc_url( get_page_link( 255 ) ); ?>" class="btn-item">Подробнее</a>

                        <?php $args = array('post_type' => 'catalog',
                                            'posts_per_page' => 40,
                                            'category_name' => 'long-products',
                                            'order' => 'DESC') ?>

                        <?php $page_index = new WP_Query($args) ?>

                        <ul class="product__item-nav">

                        <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                            <li class="product__nav-item"><a href="<?php echo esc_url( get_permalink() ); ?>" class="product__nav-link"><?php echo esc_html( the_title() ); ?></a></li>
                            
                            <?php endwhile; ?>

                        <?php endif; ?> 

                        </ul>
                        <?php wp_reset_postdata(); ?>       

                    </div>
                </div>
                <div class="product__item-wrap">
                    <div class="product__item" style="background-image: url(<?php the_field('cat-t-2', 255) ?>);">
                        <h4 class="product__item-title">ЛИСТОВОЙ ПРОКАТ</h4>
                        <a href="<?php echo esc_url( get_page_link( 255 ) ); ?>" class="btn-item">Подробнее</a>
                        
                        <?php $args = array('post_type' => 'catalog',
                                            'posts_per_page' => 40,
                                            'category_name' => 'sheet-rent',
                                            'order' => 'DESC') ?>

                        <?php $page_index = new WP_Query($args) ?>

                        <ul class="product__item-nav">

                        <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                            <li class="product__nav-item"><a href="<?php echo esc_url( get_permalink() ); ?>" class="product__nav-link"><?php echo esc_html( the_title() ); ?></a></li>
                            
                            <?php endwhile; ?>

                        <?php endif; ?> 

                        </ul>
                        <?php wp_reset_postdata(); ?>       

                    </div>
                </div>
                <div class="product__item-wrap">
                    <div class="product__item" style="background-image: url(<?php the_field('cat-t-3', 255) ?>);">
                        <h4 class="product__item-title">МЕТИЗНАЯ ПРОДУКЦИЯ</h4>
                        <a href="<?php echo esc_url( get_page_link( 255 ) ); ?>" class="btn-item">Подробнее</a>
                        
                        <?php $args = array('post_type' => 'catalog',
                                            'posts_per_page' => 40,
                                            'category_name' => 'furniture-prod',
                                            'order' => 'DESC') ?>

                        <?php $page_index = new WP_Query($args) ?>

                        <ul class="product__item-nav">

                        <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                            <li class="product__nav-item"><a href="<?php echo esc_url( get_permalink() ); ?>" class="product__nav-link"><?php echo esc_html( the_title() ); ?></a></li>
                            
                            <?php endwhile; ?>

                        <?php endif; ?> 

                        </ul>
                        <?php wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-we">
        <div class="container-1104">
            <div class="why-we__content">

                <?php if( have_rows('why-we-list') ): ?>   
                    <div class="why-we__list">
                
                    <?php while( have_rows('why-we-list') ): the_row(); 
                        $image = get_sub_field('why-we-list_img');
                        $title = get_sub_field('why-we-list_title');

                        ?>

                        <div class="why-we__item">
                            <div class="why-we__item-img">
                                <img src="<?php echo $image; ?>" alt="img">
                            </div>
                            <p><?php echo $title; ?></p>
                        </div>

                    <?php endwhile; ?>  

                    </div>
                <?php endif; ?> 

                <div class="why-we__text-content">

                    <?php if (get_field('block-title_4')) { ?>
                        <div class="main-title__wrap">
                            <h3 class="main-title"><?php the_field('block-title_4') ?></h3>
                        </div>
                    <?php } ?>  

                    <?php if (get_field('block-text_4')) { ?>
                        <p><?php the_field('block-text_4') ?></p>
                    <?php } ?>  
                    
                </div>
            </div>
        </div>
    </section>

    <section class="delivery">
        <div class="container">

            <?php if (get_field('block-title_6')) { ?>
                <div class="main-title__wrap">
                    <h3 class="main-title"><?php the_field('block-title_6') ?></h3>
                </div>
            <?php } ?> 

            <?php if (get_field('block-text_6')) { ?>
                <p class="main-title__description"><?php the_field('block-text_6') ?></p>
            <?php } ?> 

            <?php if( have_rows('cars-list', 288) ): ?>   
                <div class="product__list  delivery__list">
            
                <?php while( have_rows('cars-list', 288) ): the_row(); 
                    $img = get_sub_field('cars-list_img');
                    $title = get_sub_field('cars-list_name');
                    $price = get_sub_field('cars-list_price');

                    ?>

                    <div class="product__item-wrap">
                        <div class="product__item" style="background-image: url(<?php echo $img; ?>);">
                            <h4 class="product__item-title"><?php echo $title; ?></h4>
                            <a href="<?php echo esc_url( get_page_link( 288 ) ); ?>" class="btn-item">Подробнее</a>

                            <?php if( have_rows('cars-list_text') ): ?>   
                                <ul class="product__item-nav">
                            
                                <?php while( have_rows('cars-list_text') ): the_row(); 
                                    $text_item = get_sub_field('cars-list_text-item');

                                    ?>

                                    <li class="product__nav-item"><?php echo $text_item; ?></li>

                                <?php endwhile; ?>  

                                <li class="product__nav-item"><?php echo $price; ?></li>

                                </ul>
                            <?php endif; ?> 
                    
                        </div>
                    </div>

                <?php endwhile; ?>  

                 </div>
            <?php endif; ?> 
            
        </div>
    </section>

    <section class="reviews">

        <?php if (get_field('block-title_7')) { ?>
            <div class="main-title__wrap">
                <h3 class="main-title"><?php the_field('block-title_7') ?></h3>
            </div>
        <?php } ?>  
        
        <?php $args = array('post_type' => 'review',
                            'order' => 'DESC') ?>

        <?php $page_index = new WP_Query($args) ?>

        <div class="reviews__slider-wrap">
            <div class="reviews__slider">

            <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                <div class="reviews__item-wrap">
                    <div class="reviews__item">
                        <strong class="reviews__name"><?php esc_html( the_title() ); ?></strong>
                        <?php esc_html( the_content() ); ?>
                    </div>
                </div>
                
                <?php endwhile; ?>

            <?php endif; ?> 
            </div>
            <div class="reviews__slider-btns">
                <button class="reviews__slider-btn  reviews__slider-btn--prev">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/sl-l.svg" alt="prev">
                </button>
                <button class="reviews__slider-btn  reviews__slider-btn--next">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/sl-r.svg" alt="next">
                </button>
            </div>
        </div>
        <?php wp_reset_postdata(); ?>

    </section>

    <div class="partners">
        <div class="container">

            <?php if( have_rows('partners-list') ): ?>   
                <div class="partners__list">
            
                <?php while( have_rows('partners-list') ): the_row(); 
                    $image = get_sub_field('partners-list-img');

                    ?>

                    <div class="partners__item-wrap">
                        <div class="partners__item">
                            <img src="<?php echo $image; ?>" alt="img">
                        </div>
                    </div>
                    
                <?php endwhile; ?>  

                </div>
            <?php endif; ?> 
            
        </div>
    </div>

    <?php get_template_part( 'template-parts/contact-form' ); ?>

<?php get_footer(); ?>