<?php 
/**
 *	Template name: Customer-info Page 
 */

get_header(); ?>

    <?php get_template_part( 'template-parts/inner-title' ); ?>

    <section class="schema">
        <div class="container">

	        <?php
	        $page_text = get_the_content();
	        if ( !empty($page_text) ) { ?>

		        <?php the_content(); ?>
                <br>
                <br>

	        <?php } ?>
            
            <?php if (get_field('block-title_3', 7)) { ?>
                <div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full">
                    <h3 class="main-title"><?php the_field('block-title_3', 7) ?></h3>
                </div>
            <?php } ?>  

            <?php if( have_rows('shema-list', 7) ): ?>   
                
                <div class="schema__list">
                    
                    <?php while( have_rows('shema-list', 7) ): the_row(); 
                        $image = get_sub_field('shema-list_img');
                        $title = get_sub_field('shema-list_title');
                        $text = get_sub_field('shema-list_descr');

                        ?>

                        <div class="schema__item">
                            <div class="schema__item-img">
                                <img src="<?php echo $image; ?>" alt="img">
                            </div>
                            <div class="schema__item-text-content">
                                <strong class="schema__item-title"><?php echo $title; ?></strong>
                                <p><?php echo $text; ?></p>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                    
                </div>
            <?php endif; ?> 

        </div>
    </section>
    
    <div class="schema-partners">
        <div class="container">

            <?php if (get_field('cus-partners_title')) { ?>
                <div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full">
                    <h3 class="main-title"><?php the_field('cus-partners_title') ?></h3>
                </div>
            <?php } ?>  

            <?php if (get_field('cus-partners_text')) { ?>
                <p class="main-title__description  main-title__description--left   main-title__description--full"><?php the_field('cus-partners_text') ?></p>
            <?php } ?>  

            <?php if( have_rows('cus-partners_list') ): ?>   
                
                <div class="schema-partners__list">
                    
                    <?php while( have_rows('cus-partners_list') ): the_row(); 
                        ?>

                        <?php if( have_rows('cus-partners_list-item') ): ?>   
                            
                            <div class="schema-partners__item-wrap">
                                <div class="schema-partners__item">
                                
                                <?php while( have_rows('cus-partners_list-item') ): the_row(); 
                                    $image = get_sub_field('cus-partners_list-img');

                                    ?>

                                    <div class="schema-partners__item-img">
                                        <img src="<?php echo $image; ?>" alt="img">
                                    </div>

                                <?php endwhile; ?>  

                                </div>
                            </div>
                        <?php endif; ?> 

                    <?php endwhile; ?>  
                    
                </div>
            <?php endif; ?> 
            
        </div>
    </div>  

    <section class="faq">
        <div class="container">

            <?php if (get_field('faq_title')) { ?>
                <div class="main-title__wrap  main-title__wrap--left  main-title__wrap--full">
                    <h3 class="main-title"><?php the_field('faq_title') ?></h3>
                </div>
            <?php } ?>  

            <?php if (get_field('faq_text')) { ?>
                <p class="main-title__description  main-title__description--left   main-title__description--full"><?php the_field('faq_text') ?></p>
            <?php } ?>

            <?php if( have_rows('faq_list') ): ?>   
                
                <div class="faq__list">
                    
                    <?php while( have_rows('faq_list') ): the_row(); 
                        $title = get_sub_field('faq_title');
                        $text = get_sub_field('faq_item-text');

                        ?>

                        <div class="faq__item">
                            <div class="faq__item-title-content">
                                <strong class="faq__item-title"><?php echo $title; ?></strong>
                            </div>
                            <div class="faq__item-tab">
                                <?php echo $text; ?>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                    
                </div>
            <?php endif; ?>   
            
        </div>
    </section>

<?php get_footer(); ?>