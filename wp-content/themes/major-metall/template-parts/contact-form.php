<?php if (get_field('main-form', 'options')) { ?>
	<section class="contact-form">
		<div class="container-1104">

			<?php if (get_field('main-form_title', 'options')) { ?>
				<div class="main-title__wrap">
					<h3 class="main-title"><?php the_field('main-form_title', 'options') ?></h3>
				</div>
			<?php } ?> 

			<?php if (get_field('main-form_text', 'options')) { ?>
				<strong class="contact-form__description"><?php the_field('main-form_text', 'options') ?></strong>
			<?php } ?> 

			<div class="main-form">
				<?php the_field('main-form', 'options') ?>
			</div>
		</div>
	</section>
<?php } ?> 
