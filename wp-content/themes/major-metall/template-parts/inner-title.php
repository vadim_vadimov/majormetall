<section class="inner-title">
    <div class="container">
        <div class="inner-title__content">
            <div class="square-mask"></div>
            <h2 class="inner-title__title"><?php echo esc_html( get_the_title() ); ?></h2>
        </div>
    </div>
</section>