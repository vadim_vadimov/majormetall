<?php

// Register Custom Post Type
function custom_post_type_catalog() {

	$labels = array(
		'name'                  => 'Каталог',
		'singular_name'         => 'Каталог',
		'menu_name'             => 'Каталог',
		'add_new_item'          => 'Добавить Новый',
		'add_new'               => 'Добавить Новый',
		'new_item'              => 'Новый',
		'edit_item'             => 'Редактировать',
		'update_item'           => 'Обновить',
		'view_item'             => 'Смотреть',
		'view_items'            => 'Смотреть Все',
	);
	$rewrite = array(
		'slug'                  => 'catalog',
		'with_front'            => true,
		'pages'                 => false,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => 'Каталог',
		'description'           => 'Post Type Description',
		'labels'                => $labels,
		'supports'              => array(
			'title', 
			'editor', 
			'thumbnail',
		),
		'taxonomies'  => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-portfolio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'catalog', $args );

}
add_action( 'init', 'custom_post_type_catalog', 0 );


?>