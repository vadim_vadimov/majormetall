if($('#map-1').length > 0) {
	ymaps.ready(function () {
	    var myMap = new ymaps.Map('map-1', {
	            center: [55.803142, 37.758217],
	            zoom: 15,
	            controls: []
	        }),

	        // Создаём макет содержимого.
	        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
	            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
	        ),

	        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	            
	        }, {
	            // Опции.
	            // Необходимо указать данный тип макета.
	            iconLayout: 'default#image',
	            // Своё изображение иконки метки.
	            iconImageHref: 'http://majormetall.htmldev.pro/wp-content/themes/major-metall/assets/img/map-mark.svg',
	            // Размеры метки.
	            iconImageSize: [36, 51],
	            // Смещение левого верхнего угла иконки относительно
	            // её "ножки" (точки привязки).
	            iconImageOffset: [-15, -38]
	        });

	    myMap.geoObjects
	        .add(myPlacemark);

	    myMap.behaviors.disable('scrollZoom')    
	});
}

$(function () {
	function homeBenefitsSlider() {
		let $slider = $('.about-text-slides');
		if ($slider.length === 0) return false;
		let $indicator = $('.about__numbers-indicator');
        $slider.on('init', function () {
            $indicator.css({
				width: Math.round($('.about__num-item').eq(1).width()),
				opacity: 1
			});
			$('.about__num-item').on('click', function (e) {
				e.preventDefault();
				let $this = $(this),
					thisNum = $this.attr('data-num');
				let numWidth = Math.round($this.width()),
					positionLeft = Math.round($this.position().left);
                $indicator.css({
					left: positionLeft,
					width: numWidth
				});
				$slider.slick('slickGoTo', thisNum);
            });
        });
		$slider.slick({
            adaptiveHeight: true,
            arrows: false,
            fade: true,
			speed: 500
		});
	}
	homeBenefitsSlider();


	$('.slider-for').slick({
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	arrows: false,
	  	fade: true,
	  	asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	asNavFor: '.slider-for',
	  	dots: true,
	  	arrows: false,
	  	appendDots: '.intro__slider-btns',
	});

	$(".intro__slider-btn--prev").on("click", function() {
	    $(".slider-nav").slick("slickPrev");
	});

	$(".intro__slider-btn--next").on("click", function() {
	    $(".slider-nav").slick("slickNext");
	});

	$('.reviews__slider').slick({
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	arrows: false,
	  	centerMode: true,
	  	centerPadding: '300px',
	  	dots: true,
	  	adaptiveHeight: true,
	  	responsive: [
	  	    {
	  	        breakpoint: 1361,
	  	        settings: {
	  	            centerPadding: '200px',
	  	        }
	  	    },
	  	    {
	  	        breakpoint: 1200,
	  	        settings: {
	  	            centerPadding: '150px',
	  	        }
	  	    },
	  	    {
	  	        breakpoint: 861,
	  	        settings: {
	  	            centerPadding: '50px',
	  	        }
	  	    },
	  	    {
	  	        breakpoint: 761,
	  	        settings: {
	  	            centerPadding: '0px',
	  	        }
	  	    },
	  	]
	});

	$(".reviews__slider-btn--prev").on("click", function() {
	    $(".reviews__slider").slick("slickPrev");
	});

	$(".reviews__slider-btn--next").on("click", function() {
	    $(".reviews__slider").slick("slickNext");
	});



	$('.burger-content').on('click', function () {
	  $('.header__menu').toggleClass('active');
      $('.burger-mob').toggleClass('active');

	  $('body').toggleClass('overflowHidden');
	});


	$('.faq__item-title-content').on('click', function() {
		$(this).toggleClass('active');
  		$(this).next('.faq__item-tab').slideToggle();
	});

  
});