if($('#map-1').length > 0) {
	ymaps.ready(function () {
	    var myMap = new ymaps.Map('map-1', {
	            center: [55.803142, 37.758217],
	            zoom: 15,
	            controls: []
	        }),

	        // Создаём макет содержимого.
	        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
	            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
	        ),

	        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	            
	        }, {
	            // Опции.
	            // Необходимо указать данный тип макета.
	            iconLayout: 'default#image',
	            // Своё изображение иконки метки.
	            iconImageHref: 'img/map-mark.svg',
	            // Размеры метки.
	            iconImageSize: [36, 51],
	            // Смещение левого верхнего угла иконки относительно
	            // её "ножки" (точки привязки).
	            iconImageOffset: [-15, -38]
	        });

	    myMap.geoObjects
	        .add(myPlacemark);

	    myMap.behaviors.disable('scrollZoom')    
	});
}

$(function () {

	$('.slider-for').slick({
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	arrows: false,
	  	fade: true,
	  	asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	asNavFor: '.slider-for',
	  	dots: true,
	  	arrows: false,
	  	appendDots: '.intro__slider-btns',
	});

	$(".intro__slider-btn--prev").on("click", function() {
	    $(".slider-nav").slick("slickPrev");
	});

	$(".intro__slider-btn--next").on("click", function() {
	    $(".slider-nav").slick("slickNext");
	});

	$('.reviews__slider').slick({
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	arrows: false,
	  	centerMode: true,
	  	centerPadding: '300px',
	  	dots: true,
	  	adaptiveHeight: true,
	  	responsive: [
	  	    {
	  	        breakpoint: 1361,
	  	        settings: {
	  	            centerPadding: '200px',
	  	        }
	  	    },
	  	    {
	  	        breakpoint: 1200,
	  	        settings: {
	  	            centerPadding: '150px',
	  	        }
	  	    },
	  	    {
	  	        breakpoint: 861,
	  	        settings: {
	  	            centerPadding: '50px',
	  	        }
	  	    },
	  	    {
	  	        breakpoint: 761,
	  	        settings: {
	  	            centerPadding: '0px',
	  	        }
	  	    },
	  	]
	});

	$(".reviews__slider-btn--prev").on("click", function() {
	    $(".reviews__slider").slick("slickPrev");
	});

	$(".reviews__slider-btn--next").on("click", function() {
	    $(".reviews__slider").slick("slickNext");
	});



	$('.burger-content').on('click', function () {
	  $('.header__menu').toggleClass('active');
      $('.burger-mob').toggleClass('active');

	  $('body').toggleClass('overflowHidden');
	});


	$('.faq__item-title-content').on('click', function() {
		$(this).toggleClass('active');
  		$(this).next('.faq__item-tab').slideToggle();
	});



	function pageWidget(pages) {
        var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
        widgetWrap.prependTo("body");
        for (var i = 0; i < pages.length; i++) {
            $('<li class="widget_item"><a class="widget_link" href="' + pages[i][0] + '.html' + '">' + pages[i][1] + '</a></li>').appendTo('.widget_list');
        }
        var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:5px 10px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_list{padding: 0;}.widget_item{list-style:none;padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
        widgetStilization.prependTo(".widget_wrap");
    }

    pageWidget([
        ['index', 'Главная'],
        ['about', 'О компании'],
        ['catalog', 'Каталог'],
        ['single', 'Товар'],
        ['service', 'Услуги'],
        ['buyer', 'Покупателю'],
        ['contacts', 'Контакты'],
    ]);
  
});